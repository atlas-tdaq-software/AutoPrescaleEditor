# AutoPrescaleEditor

[AutoPrescaler TWiki](https://twiki.cern.ch/twiki/bin/view/Atlas/TriggerOnlineAutoPrescaler)

## Scripts
- start_AutoPrescaleEditor - script to Run APE at P1
- start_AutoPrescaleEditor_HLTPatchTest - script to Run APE at P1 using the jar installed in the HLT Patch Area
- test_AutoPrescaleEditor_DatabaseUpload - script to test the database extraction and upload of new prescale set