export PARTITION=part_l2ef_stelzer
export ISSERVER=apetest
export TRIGGERDATABASE=TRIGGERDBJOERG
export ISNAME=PrescaleRequest
export SERVANT=L1AutoPrescaler

export CORAL_DBLOOKUP_PATH=${TestArea}/
export CORAL_AUTH_PATH=$CORAL_DBLOOKUP_PATH

unset TRIGGER_EXP_CORAL_PATH

export TDAQ_ERS_ERROR="lstdout,mrs"
export TDAQ_ERS_DEBUG="lstdout,mrs"
export TDAQ_ERS_WARNING="lstdout,mrs"
export TDAQ_ERS_FATAL="lstdout,mrs"
export TDAQ_ERS_INFORMATION="lstdout,mrs"

export TDAQ_ERS_DEBUG_LEVEL="2"
export TDAQ_ERS_VERBOSITY="2"

JAVA="/afs/cern.ch/atlas/project/tdaq/inst/sw/lcg/external/Java/JDK/1.6.0/ia32/bin/java -Xms64m -Xmx1024m"
CP="-cp ${TDAQ_INST_PATH}/share/lib/IguiCommander.jar:${TestArea}/InstallArea/share/lib/AutoPrescaleEditor.jar:${TestArea}/InstallArea/share/lib/TriggerTool.jar:$CLASSPATH"


if [[ $1 = 'start' ]]; then

    rm -r output
    mkdir -p output
    mkdir -p run
    
    read pid < run/ape.pid
    pidcom=$(ps hp $pid o %c)
    es=$?
    if [[ $es = 0 && $pidcom = java ]]; then
        kill $pid > /dev/null
    fi
    
    #is_server -p $PARTITION -n $ISSERVER & > /dev/null
        
    $JAVA $CP autoprescaleeditor.AutoPrescaleEditor -p $PARTITION -n $ISSERVER.$PrescaleRequest -db $TRIGGERDATABASE -c MaxItemRates.txt -s $SERVANT -smk 6 -testmode 
    #&>! output/ape.log &
    echo $! >! run/ape.pid

elif [[ $1 = 'reset' ]]; then

    $JAVA $CP autoprescaleeditor.TestAutoPrescaling -p $PARTITION -db $TRIGGERDATABASE -n $ISSERVER.$PrescaleRequest -reset &>! output/reset.log &

    
elif [[ $1 = 'send' ]]; then

    $JAVA $CP autoprescaleeditor.TestAutoPrescaling -p $PARTITION -db $TRIGGERDATABASE -n $ISSERVER.$PrescaleRequest -c MaxItemRates.txt -l 5 &>! output/send.log &

elif [[ $1 = 'is' ]]; then

    $JAVA $CP autoprescaleeditor.TestAutoPrescaling -p $PARTITION -db $TRIGGERDATABASE -n $ISSERVER.$PrescaleRequest -isupdate

elif [[ $1 = 'block' ]]; then

    #$JAVA $CP autoprescaleeditor.TestAutoPrescaling -p $PARTITION -block
    rc_command_autoprescaler -p $PARTITION -n $SERVANT HOLD

elif [[ $1 = 'resume' ]]; then

    #$JAVA $CP autoprescaleeditor.TestAutoPrescaling -p $PARTITION -resume
    rc_command_autoprescaler -p $PARTITION -n $SERVANT RESUME

fi
