package autoprescaleeditor;

import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.HelpFormatter;

import autoprescaleeditor.menu.L1Prescale;

/*
 Test the menu/ps set extraction, editing of the set and uploading

 Run with test_AutoPrescaleEditor_DatabaseUpload
*/
public class TestDatabaseUpload {

    // Logger
    private static final APELogger Logger = APELogger.createLogger("testDatabaseUpload");

    public static void main( String[] args ) {
        Logger.info("Starting the database upload test");

        String triggerdb  = "TRIGGERDBATN";
        Integer smk = 487;
        Integer l1ps = 400;

        CommandLine cmd = null;
        Options options = new Options();

        try {
            CommandLineParser parser = new DefaultParser(); // PosixParser();
            options.addOption("h", false, "help");
            options.addOption("db", true, "triggerdb ["+triggerdb+"]");
            options.addOption("smk", true, "SuperMasterKey ["+smk+"]");
            options.addOption("l1ps", true, "L1 PresclaeSetKey ["+l1ps+"]");
            cmd = parser.parse(options, args);
        } catch (ParseException ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("SendL1Pss2IS <options>", options);
            System.exit(1);
        }
        if (cmd.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("SendL1Pss2IS <options>", options);
            System.exit(0);
        }

        if (cmd.hasOption("smk"))     { smk    = Integer. parseInt(cmd.getOptionValue("smk")); }
        if (cmd.hasOption("db"))  { triggerdb  = cmd.getOptionValue("db"); }
        if (cmd.hasOption("l1ps"))   { l1ps    = Integer. parseInt(cmd.getOptionValue("l1ps")); }

        Logger.info("Running the test with smk " + smk + ", l1ps " + l1ps + " and db " + triggerdb);

        // Configure the output directory for the json files to be current directory
        AutoPrescaleEditor.OutputDir = System.getProperty("user.dir") + "/";
        Logger.info("Files from the database will be saved to " +  AutoPrescaleEditor.OutputDir);

        // Read the prescale set from the database
        Logger.info("Configuring the Prescale Saver");
        L1PrescaleSaver prescaleSaver = new L1PrescaleSaver(triggerdb, new ArrayList<String>());
        prescaleSaver.configureWithSMK(smk);

        Logger.info("Reading the L1 Prescales set");
        L1Prescale pss = prescaleSaver.readL1PSS(l1ps);

        StringBuilder comment = new StringBuilder("AutoPrescaler Database Upload test. Original key ").append(l1ps).append(" Prescaled items: ");

        // List of items that can possibly be changes by APE 
        // based on https://gitlab.cern.ch/atlas-tdaq-oks/p1/tdaq-09-04-00/-/blob/master/daq/segments/AutoPrescaler/AutoPrescalerRules.data.xml
        String itemsToChange [] = { "L1_ZB", "L1_EM3_EMPTY", "L1_EM7_EMPTY", "L1_EM7_FIRSTEMPTY", 
                                    "L1_J12_BGRP12", "L1_J12_EMPTY", "L1_J12_FIRSTEMPTY", "L1_J12_UNPAIRED_ISO",
                                    "L1_J30p31ETA49_EMPTY", "L1_J30p31ETA49_UNPAIRED_ISO", "L1_J30p31ETA49_UNPAIRED_NONISO",
                                    "L1_TAU8_EMPTY", "L1_TAU8_FIRSTEMPTY", "L1_TAU8_UNPAIRED_ISO" }; 

        Random random = new Random();
        for (String itemName: itemsToChange) {
            int oldCut = pss.getCutValue(itemName);
            if (oldCut < 0) continue;

            int newCut  = random.nextInt(0xFFFFFF);

            pss.setCutValue(itemName, newCut);
            Logger.info("Setting cut of item " + itemName + " to " + newCut);
            comment.append(itemName).append(", ");
        }

        pss.setComment(comment.toString());

        // Upload
        int newL1Psk = prescaleSaver.saveNewL1PrescaleSet(pss);
        Logger.info("New L1 PSK: " + newL1Psk);
        System.exit(0);
    }


} 