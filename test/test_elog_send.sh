export PARTITION=part_l2ef_stelzer
export ISSERVER=apetest
export TRIGGERDATABASE=TRIGGERDBJOERG
export ISNAME=PrescaleRequest
export SERVANT=L1AutoPrescaler

export CORAL_DBLOOKUP_PATH=${TestArea}/
export CORAL_AUTH_PATH=$CORAL_DBLOOKUP_PATH

unset TRIGGER_EXP_CORAL_PATH

export TDAQ_ERS_ERROR="lstdout,mrs"
export TDAQ_ERS_DEBUG="lstdout,mrs"
export TDAQ_ERS_WARNING="lstdout,mrs"
export TDAQ_ERS_FATAL="lstdout,mrs"
export TDAQ_ERS_INFORMATION="lstdout,mrs"

export TDAQ_ERS_DEBUG_LEVEL="2"
export TDAQ_ERS_VERBOSITY="2"

JAVA="/afs/cern.ch/atlas/project/tdaq/inst/sw/lcg/external/Java/JDK/1.6.0/ia32/bin/java -Xms64m -Xmx1024m"
CP="-cp ${TDAQ_INST_PATH}/share/lib/IguiCommander.jar:${TestArea}/InstallArea/share/lib/AutoPrescaleEditor.jar:${TestArea}/InstallArea/share/lib/TriggerTool.jar:$CLASSPATH"

$JAVA $CP autoprescaleeditor.TestAutoPrescaling -p $PARTITION -db $TRIGGERDATABASE -n $ISSERVER.$PrescaleRequest -elog ./elogpw.txt 
