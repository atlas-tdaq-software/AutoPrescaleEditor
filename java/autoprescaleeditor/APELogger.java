package autoprescaleeditor;

import java.io.IOException;

import java.io.StringWriter;
import java.io.PrintWriter;

import java.util.Date;
import java.text.SimpleDateFormat;

import ers.Logger;


// Logger with possibility to log stacktrace and send igui messages
public class APELogger {
    // Wrappers for the ers Issues
    @SuppressWarnings("serial")
    static public class TestISIssue extends ers.Issue {
        public TestISIssue(final String message) { super(message); }
    }

    @SuppressWarnings("serial")
    static public class Issue extends ers.Issue {
        public Issue(String message) { super(message); }
    }

    @SuppressWarnings("serial")
    static public class LBIssue extends Issue {
        public LBIssue(String message) { super(message); }
    }

    @SuppressWarnings("serial")
    static public class ISIssue extends Issue {
        public ISIssue(String message) { super(message); }
    }

    @SuppressWarnings("serial")
    static public class PSIssue extends Issue {
        public PSIssue(String message) { super(message); }
    }

    @SuppressWarnings("serial")
    static public class ConfigIssue extends Issue {
        public ConfigIssue(String message) { super(message); }
    }

    @SuppressWarnings("serial")
    static public class ControlIssue extends Issue {
        public ControlIssue(String message) { super(message); }
    }

    @SuppressWarnings("serial")
    static public class SimIssue extends Issue {
        public SimIssue(String message) { super(message); }
    }

    // Issues related to the database access
    @SuppressWarnings("serial")
    static public class DatabaseIssue extends Issue {
        public DatabaseIssue(String message) { super(message); }
    }

    // Issues related to file reading/writing
    @SuppressWarnings("serial")
    static public class IOIssue extends Issue {
        public IOIssue(String message) { super(message); }
    }


    private String name;
    private Level level = Level.INFO;

    @SuppressWarnings("PackageVisibleInnerClass")
    enum Level { ERROR, WARNING, INFO, DEBUG }

    private static String format = "%s %-25s %-10s %s\n";

    public static APELogger createLogger (String name) {
        return new APELogger(name);
    }

    protected APELogger(String name) {
        this.name = name;
        this.level = Level.DEBUG;
    }

    private void printLog(Level level, String message){
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        System.out.printf(format, timeStamp, this.name, level.name(), message);
    }

    public void info(String message) {
        printLog(Level.INFO, message);
    }

    // TODO add log level setting
    public void debug(String message) {
        if (this.level == Level.DEBUG){
            printLog(Level.DEBUG, message);
        }
    }

    public void infoErs(final ers.Issue i) {
        ers.Logger.info(i);
    }

    public void warningErs(final ers.Issue i) {
        ers.Logger.warning(i);
    }

    public void errorErs(final ers.Issue i) {
        ers.Logger.error(i);
    }

    public void fatalErs(final ers.Issue i) {
        ers.Logger.fatal(i);
    }

    public void logStacktrace(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        System.out.println(sw.toString());
    }
}