package autoprescaleeditor;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;
import java.lang.Process;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.File;


// Class to provide access to the Trigger database by executing the python scripts
class DatabaseAccess {

    class DatabaseQueryResult {
        public List<String> stdout = new ArrayList<String>();
        public List<String> stderr = new ArrayList<String>();
    }

    // Logger
    private static final APELogger Logger = APELogger.createLogger("DatabaseAccess");

    /**
     * Build Single string out of the database query output passed as array of strings
     *      can be used for both stdout and stderr
     *
     * @param inputStream: ArrayList with strings
     *
     * @return Strign with message filename
     **/
    private static String buildMsgFromStream(List<String> inputStream) {
        String msg = "";
        for (String inputMsg : inputStream) {
            msg += inputMsg + "\n";
        }
        return msg;
    }

    private int m_smk = -1;
    private String m_dbAlias = "TRIGGERDB_RUN3";

    public DatabaseAccess(int newSmk, String newDbAlias) {
        this.m_smk = newSmk;
        this.m_dbAlias = newDbAlias;
    }

    /**
     * Create and schedule a query to retrieve L1Menu based on the smk and dbalias
     *
     * @return L1Menu filename
     **/
    public String retrieveL1Menu(){
        Logger.debug(String.format("Retrieving l1menu for smk %d dbalias %s", this.m_smk, this.m_dbAlias));
        String[] query = new String[] {"extractMenu.py", "--l1only", "--smk",  Integer.toString(this.m_smk), "--dbalias", this.m_dbAlias};
        DatabaseQueryResult queryOutput = databaseQuery(query);

        if (queryOutput.stderr.isEmpty() == false) {
            Logger.errorErs(new APELogger.DatabaseIssue("Error during query execution. The output was:\n" + DatabaseAccess.buildMsgFromStream(queryOutput.stderr)));
            return "";
        }

        // Find the filename in the command output
        for (String outMessage : queryOutput.stdout) {
            if (outMessage.contains("L1Menu")) {
                Logger.debug("Retrieving l1menu success");
                return AutoPrescaleEditor.OutputDir + outMessage.replace("Wrote file ", "");
            }
        }

        return "";  
    }


    /**
     * Create and schedule a query to retrieve L1Prescales set
     *  based on smk, dbalias and passed l1 prescale key
     *
     * @param l1Psk: L1 prescales set key
     *
     * @return L1PrescalesSet filename
     **/
    public String retrieveL1Prescales(int l1Psk) {
        // TODO 2 change input to readonly
        Logger.debug(String.format("Retrieving prescales for l1psk %d smk %d dbalias %s", l1Psk, this.m_smk, this.m_dbAlias));
        String[] query = new String[] {"extractPrescales.py", "--l1psk", Integer.toString(l1Psk), "--dbalias", this.m_dbAlias};
        DatabaseQueryResult queryOutput = databaseQuery(query);
        
        if (queryOutput.stderr.isEmpty() == false) {
            Logger.errorErs(new APELogger.DatabaseIssue("Error during query execution. The output was:\n" + DatabaseAccess.buildMsgFromStream(queryOutput.stderr)));
            return "";
        }

        // Find the filename in the command output
        for (String outMessage : queryOutput.stdout) {
            if (outMessage.contains("Wrote file")) {
                Logger.debug("Retrieving l1ps success");
                return AutoPrescaleEditor.OutputDir + outMessage.replace("Wrote file ", "");
            }
        }

        Logger.errorErs(new APELogger.DatabaseIssue("Feedback from database not found. Output was: \n" + DatabaseAccess.buildMsgFromStream(queryOutput.stdout)));
        return "";        
    }

    /**
     * Create and schedule a query to insert the L1 Prescales set file to the database
     *  for smk and dbalias
     *
     * @param l1PSkFile: Name of the file to upload 
     * @param comment: Upload comment
     *
     * @return L1 Prescale key of the inserted set
     **/
    public int insertL1Prescales(String l1PskFile, String comment) {
        Logger.debug(String.format("Inserting prescales from file %s", l1PskFile));
        String[] query = new String[] {"insertPrescales.py", "--l1ps",  l1PskFile, "--smk", Integer.toString(this.m_smk), "--dbalias", this.m_dbAlias, "--comment", comment};

        DatabaseQueryResult queryOutput = databaseQuery(query);

        if (queryOutput.stderr.isEmpty() == false){
            Logger.errorErs(new APELogger.DatabaseIssue("Error during query execution. The output was:\n" + DatabaseAccess.buildMsgFromStream(queryOutput.stderr)));
            return -1;
        }

        // Find the key
        for (String outMessage : queryOutput.stdout) {
            // This key was already in the database
            if (outMessage.contains("The L1 prescale set is already contained")) {
                Logger.info("This prescale set is already in the database");
                return Integer.parseInt(outMessage.replace("L1", "").replaceAll("[^0-9]", ""));

            } else if (outMessage.contains("Inserted L1 prescale set")) {
                Logger.info("Inserted new key");
                return Integer.parseInt(outMessage.substring(outMessage.indexOf("with index ") + "with index ".length(), outMessage.indexOf(", linked to")));
            }
        }

        return 0;
    }

    /**
     * Execute the database query and return the standard output and error stream
     *     the query will be run in subprocess
     *
     * @param query: Query to execute
     *
     * @return standard output and error wrapped in DatabaseQueryResult object
     **/
    DatabaseQueryResult databaseQuery(String[] query) {
        // TODO 1 ecexute queries in subriectory
        Logger.debug(String.format("Query: %s", convertStringArrayToString(query, " ")));
        DatabaseQueryResult result = new DatabaseQueryResult();
        
        String s = null;
        try {
            File dir = new File(AutoPrescaleEditor.OutputDir);
            Process p = Runtime.getRuntime().exec(query, null, dir);
            
            BufferedReader stdInput = new BufferedReader(new 
                 InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new 
                 InputStreamReader(p.getErrorStream()));

            // read the output from the command
            while ((s = stdInput.readLine()) != null) {
                result.stdout.add(s);
            }
            
            // read any errors from the attempted command
            while ((s = stdError.readLine()) != null) {
                result.stderr.add(s);
            }
        }
        catch (IOException ex) {
            Logger.errorErs(new APELogger.DatabaseIssue("Exception in query execution: \n" + ex.toString()));
            Logger.logStacktrace(ex);
            System.exit(-1);
        }

        Logger.debug("Result of the query:\n\nSTDOUT:\n" + DatabaseAccess.buildMsgFromStream(result.stdout) 
                        + "\nSTDERR:" + DatabaseAccess.buildMsgFromStream(result.stderr) + "\nEnd of query output");
        return result;  
    }


	private static String convertStringArrayToString(String[] strArr, String delimiter) {
        StringBuilder builder = new StringBuilder();
        for (String s : strArr) {
            builder.append(s).append(delimiter);
        }
        return builder.substring(0, builder.length() - 1); // Remove last delimiter
	}

}