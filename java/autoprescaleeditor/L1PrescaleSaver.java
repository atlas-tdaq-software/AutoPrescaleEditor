package autoprescaleeditor;

import java.util.List;
import java.math.BigDecimal;

import autoprescaleeditor.menu.L1Item;
import autoprescaleeditor.menu.L1Prescale;
import autoprescaleeditor.menu.L1Menu;

import autoprescaleeditor.DatabaseAccess;

import java.util.HashMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;


/**
 * Class responsible for reading, validating and saving the L1 Prescales sets
 *  It has associated L1Menu - it won't change during the run
 */
public class L1PrescaleSaver {

    /**
     * Request validity states
     */
    @SuppressWarnings("PackageVisibleInnerClass")
    enum RequestState { VALID, VALID_HIGH_RATE, CANCELED_REQUEST, INVALID_SIZE, EMPTY_REQUEST, FORBIDDEN_ITEM }

    static public Boolean isPrescaleSetValid (RequestState state){
        return (state == L1PrescaleSaver.RequestState.VALID 
             || state == L1PrescaleSaver.RequestState.VALID_HIGH_RATE
             || state == L1PrescaleSaver.RequestState.EMPTY_REQUEST);
    }

    static public Boolean isPrescaleSetValid (ValidationResult request){
        return isPrescaleSetValid(request.getState());
    }
    
    // Validation result: state and new, corrected prescales
    public class ValidationResult {
        RequestState state;
        L1Prescale l1ps;

        public ValidationResult (RequestState r, L1Prescale p){
            this.state = r;
            this.l1ps = p;
        }

        public RequestState getState() {return this.state;}

        public L1Prescale getPrescaleSet() {return this.l1ps;}
    }

    // Logger
    private static final APELogger Logger = APELogger.createLogger("L1PrescaleSaver");
    
    // List of items that are allowed to change prescales, should be moved to OKS
    final List<String> itemsAllowedToChange;

    // Current L1 menu and SMK. Will not change during the run 
    private L1Menu currentL1Menu = null;
    private int loadedSmk = 0;

    private String triggerdb = "";

    // Map from CTP_ID to Item name     
    private Map<Integer,String> ctpIdToItemNameMap = new HashMap<Integer,String>();

    // ctpIdToItemNameMap getter
    public Map<Integer, String> getCtpIdToItemNameMap() {
        return Collections.unmodifiableMap(ctpIdToItemNameMap);
    }

    // Convert the array of ctpIds to item names
    private List<String> ctpIdListToItemNameList (List<Integer> ctpIdList) {
        if (ctpIdToItemNameMap.isEmpty()){
            Logger.errorErs(new APELogger.ConfigIssue("CtpId to item name map not available! Read the L1Menu first"));
            return new ArrayList<>();
        }

        List<String> itemNameList = new ArrayList<>();

        for (Integer ctpId : ctpIdList) {
            if (!ctpIdToItemNameMap.containsKey(ctpId)){
                Logger.warningErs(new APELogger.Issue("Item name for ctpid " + ctpId + "not found"));
                continue;
            } else {
               itemNameList.add(ctpIdToItemNameMap.get(ctpId));
            }
        }

        return itemNameList;
    }

    private void abortOnException(Exception ex) {
        Logger.errorErs(new APELogger.Issue("Aborting on exception " + ex.toString()));
        Logger.logStacktrace(ex);
        System.exit(-1);
    }

    /**
     * Constructor
     *
     * @param triggerdb: Alias of trigger database
     * @param items: list of items that are allowed to change used in the ps set validation
     **/
    public L1PrescaleSaver(final String triggerdb, final List<String> items) {
        this.itemsAllowedToChange = items;
        this.triggerdb = triggerdb;
    }


    /**
     *  Reads the L1 menu for given smk
     *
     * @param smk: SuperMasterKey
     **/
    public void configureWithSMK(final int smk) {
        // during tests only
        if ( smk == 0 ) { return; }
        if ( smk != this.loadedSmk ) {
            Logger.info("Reading L1 menu for SMK " + smk);
            this.readL1Menu(smk);
            this.loadedSmk = smk;
        } else {
            Logger.info("L1 menu for SMK " + smk + " was already read.");
        }
    }

    /**
     * Reading the L1 menu from the trigger DB sand saves it  
     * as current menu. Fills a map ctp -> itemName
     *
     * @param smk SuperMasterKey
     *
     ***/
    private void readL1Menu(final int smk) {
        Logger.debug("Reading L1 Menu fron the database");
        // Execute database query
        DatabaseAccess dbAccess = new DatabaseAccess(smk, this.triggerdb);
        String l1MenuFilename = dbAccess.retrieveL1Menu();

        Logger.debug("The L1 menu are saved to file " + l1MenuFilename);

        // Read the JSON
        this.currentL1Menu = new L1Menu(l1MenuFilename);
        List<L1Item> items = this.currentL1Menu.getItems();

        // Fill in the map
        Logger.debug("Create item name to ctpid map");
        ctpIdToItemNameMap.clear();
        for(L1Item it: items) {
            Logger.debug("       Name: " +  it.getName() + " id " + it.getCtpId());
            ctpIdToItemNameMap.put(it.getCtpId(), it.getName());
        }
    }

    /**
     *  Reads the L1 prescales for given key
     * 
     * @param l1Psk: L1 prescales key
     *
     * @return L1Prescales set
     **/
    public L1Prescale readL1PSS(final int l1Psk) {
        Logger.info("Reading L1 prescales set for key " + l1Psk);
        // Database connection
        DatabaseAccess dbAccess = new DatabaseAccess(this.loadedSmk, this.triggerdb);
        String l1PskFilename = dbAccess.retrieveL1Prescales(l1Psk);

        Logger.debug("The prescales are saved to file " + l1PskFilename);
        L1Prescale loadedPss= new L1Prescale(l1PskFilename, ctpIdToItemNameMap);
        return loadedPss;
    }    

    /**
     *  Upload the new L1 prescales set to the db
     *
     * @param pss: Prescale set to save
     *
     * @return New prescales key
     **/
    public int saveNewL1PrescaleSet(L1Prescale pss) {
        String comment = pss.getComment();
        Logger.debug("Saving L1 PSS with comment:\n" + comment);

        final String filename = pss.saveToFile();

        if (filename.isEmpty()){
            return -1;
        }

        // Database connection
        DatabaseAccess dbAccess = new DatabaseAccess(this.loadedSmk, this.triggerdb);
        final int newl1psk = dbAccess.insertL1Prescales(filename, comment);
        Logger.debug("New L1 PSK key is " + newl1psk);

        return newl1psk >= 0 ? newl1psk : -1;
    }

    
    /**
     * Validates a new L1 prescale set with respect to the current set
     * are allowed. Modifies the current prescale set in memory,
     * rather then creating a new one.
     *
     * @param newPss: new L1 prescales 
     * @param pssRef: original L1 prescales
     *
     * @return boolean true if the changes are valid and new prescales set
     **/
    ValidationResult validateNewPrescales(final L1Prescale newPss, final L1Prescale pssRef) {
        Logger.debug("Validate new prescales request compared to L1 pss " +  pssRef.getId() );

        final List<String> forbiddenChanges = new ArrayList<>(); // not used currenty, items not allowed to change 
        final List<Integer> updatedItemsIds = new ArrayList<>(); // only contains ids of changed items
        final L1Prescale prescaleSuggestions = new L1Prescale(pssRef);

        // This loop might be needed for forbidden items
        // Currently only used for printout and filling the lists of items that change
        for (L1Item item : currentL1Menu.getItems()) {
            Integer ctpId = item.getCtpId();
            String itemName = item.getName();
            Integer currentPs = pssRef.getCutValue(ctpId);
            Integer newPs = newPss.getCutValue(ctpId);

            // Skip unassigned item names - should now happen now
            if (itemName == null) { continue; }

            if (itemName.equals("L1_J30_EMPTY")) {
                Logger.debug("Example: " + itemName + ": " + currentPs + " to new " + newPs);
            }
            
            // No change for this item
            if (currentPs.equals(newPs)) { continue; }

            Logger.info("New prescale: " + itemName + ": " + currentPs + " to new " + newPs + " ctpid is " + ctpId);

	        // This protection isn't needed anymore, though we might want to think about it
            // TODO implement reverse logic - pass the list of forbidden items rather than allowed items
            //if (!itemsAllowedToChange.contains(itemName)) { 
                // item is not allowed to change (the CTP should make sure this does not happen)
                //forbiddenChanges.add(itemName);
                //continue;
            //}

            updatedItemsIds.add(ctpId);
            prescaleSuggestions.setCutValue(ctpId, newPs);
        }

        Logger.debug("Items to change " + ctpIdListToItemNameList(updatedItemsIds).toString());

        // TODO - change to SAME REQUEST
        if (updatedItemsIds.isEmpty()) {
            Logger.infoErs(new APELogger.Issue("The request was empty!"));
            // We want to return the same key
            return new ValidationResult(RequestState.EMPTY_REQUEST, new L1Prescale());
        }

        if (!forbiddenChanges.isEmpty()) {
            Logger.warningErs(new APELogger.PSIssue("The request contains forbidden changes"));
            return new ValidationResult(RequestState.FORBIDDEN_ITEM, new L1Prescale());
        }

        Logger.info("The prescale set has been validated. " + updatedItemsIds.size() + " items will be udpated: " + ctpIdListToItemNameList(updatedItemsIds).toString());
        
        // Prepare the comment and check for high rates
        RequestState request = RequestState.VALID;
        StringBuilder comment = new StringBuilder("Original key ").append(pssRef.getId()).append(" Prescaled items: ");

        // Iterate through changed items and create a comment and check if any of the rates was very high
        for (Integer ctpId : updatedItemsIds) {
            String itemName = ctpIdToItemNameMap.get(ctpId);
            Integer newCut = prescaleSuggestions.getCutValue(ctpId);
            Integer oldCut = pssRef.getCutValue(ctpId);

            if (L1Prescale.calculatePrescaleFromCut(newCut).doubleValue() > 300000) { // Should compare to PS not cut
                request = RequestState.VALID_HIGH_RATE;
            }
            
            comment.append(itemName).append(", ");
        }
        
        prescaleSuggestions.setComment(comment.toString());
        prescaleSuggestions.setId(-1); // Set to not be the initial key

        return new ValidationResult (request, prescaleSuggestions);
    }

    
    /**
     * Returns a prescale value that is greater or equal the requested one and a power of "step" 
     *
     * @param exact_ps: prescale value to round
     * @param step: step to round the prescales to
     *
     * @return rounded prescale value
     */
    private Integer roundup(final Integer exact_ps,
                               final int step) {
        int finalps = 1;
        while (finalps < exact_ps) {finalps *= step;}
        return finalps;
    }
    
    /**
     * Returns L1Prescale object created from a array of cut values 
     * TODO: WILL NOT WORK - item name to id map is not set. is it used though?
     *
     * @param newPss: array of cut values
     *
     * @return new L1Prescale object
     */
    static public L1Prescale asL1PSNumberVector(final Integer[] newPss) {
        L1Prescale pss = new L1Prescale();
        int ctpId = 0;
        for (Integer ps: newPss) { 
            pss.setCutValue(ctpId++, ps); 
            if ( ctpId == L1Prescale.MAX_LENGTH ) break;
        }
        return pss;
    }
}
