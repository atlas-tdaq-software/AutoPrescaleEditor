package autoprescaleeditor;

import java.sql.SQLException;

// Used by APEAbsISListener
public interface UpdateInfoT<T> {
   public void updateInfo(final T info) throws SQLException;
}

