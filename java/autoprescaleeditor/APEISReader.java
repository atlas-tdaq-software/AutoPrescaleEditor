package autoprescaleeditor;

import is.NamedInfo;
import is.Repository;

import TTCInfo.CtpControllerL1PSKey;
import TTCInfo.TrigConfSmKeyNamed;


// Class to read and publish information to IS
class APEISReader {
        
    static enum READYSTATE { STANDBY, PHYSICS, UNKNOWN }

    // Logger
    private static final APELogger Logger = APELogger.createLogger("APEISReader");

    //Partition name
    private ipc.Partition m_partition;

    /**
     * Helper class to parse to IS Boolean values
     */     
    private class BooleanInfo extends NamedInfo
    {
        boolean Ready4Physics;
        public BooleanInfo( ipc.Partition partition, String name ) {
            super( partition, name, "Boolean" );
        }
        @Override
        public void publishGuts( is.Ostream out ){
            super.publishGuts( out );
            out.put( Ready4Physics );
        }
        @Override
        public void refreshGuts( is.Istream in ){
            super.refreshGuts( in );
            Ready4Physics = in.getBoolean();
        }
    }

     /**
     * Constructor
     *
     *  @param partition: partition object
     */ 
    public APEISReader (ipc.Partition partition) {
        this.m_partition =  partition;
    }


     /**
     * Read the state from IS: RunParams.Ready4Physics
     *
     *  @return Read state
     */      
    public READYSTATE getCurrentStateFromIS()
    {
        Repository isRepository = new Repository(this.m_partition);
        BooleanInfo bi = new BooleanInfo(this.m_partition, "RunParams.Ready4Physics");
        try {
            isRepository.getValue("RunParams.Ready4Physics", bi);
            Boolean physics = bi.Ready4Physics;
            return physics ? READYSTATE.PHYSICS : READYSTATE.STANDBY;
        }
        catch(is.InfoNotFoundException|is.RepositoryNotFoundException|is.InfoNotCompatibleException ex) {
            Logger.errorErs(new APELogger.ISIssue("Did not find RunParams.Ready4Physics. Can't reset L1 PSK. " + ex.toString()));
            Logger.logStacktrace(ex);
        }
        return READYSTATE.UNKNOWN;
    }

    /**
     * Read L1 Prescale Set key from the IS: RunParams.[Physics|Standby].L1PsKey
     *
     *  @return Read L1 Prescale Set key
     */   
    public int getL1PskForCurrentStateFromIS()
    {
        Repository isRepository = new Repository(this.m_partition);

        READYSTATE rs = getCurrentStateFromIS();
        if(rs == READYSTATE.UNKNOWN) { return 0; }

        final String _isname = "RunParams." + (rs == READYSTATE.PHYSICS ? "Physics" : "Standby") + ".L1PsKey";
        TrigConfL1PsKeyNamed isl1key = new TrigConfL1PsKeyNamed(this.m_partition, _isname);
        try {
            isRepository.getValue(_isname, isl1key);
            int l1_key = isl1key.L1PrescaleKey;
            return l1_key;
        }
        catch(is.InfoNotFoundException|is.RepositoryNotFoundException|is.InfoNotCompatibleException ex) {
            Logger.errorErs(new APELogger.ISIssue("Did not find " + _isname + ". Can't reset L1 PSK. " + ex.toString()));
            Logger.logStacktrace(ex);
        }
        return 0;
    }


    /**
     * Read CTP loaded L1 Prescale Set key from the IS: L1CT.CtpControllerLoadedL1PSKey
     *
     *  @return Read L1 Prescale Set key
     */  
    public int getCurrentL1PskFromIS()
    {
        try {
            CtpControllerL1PSKey isl1key = new CtpControllerL1PSKey(); //this.partition, "L1CT.CtpControllerLoadedL1PSKey");
            //TrigConfL1PsKeyNamed isl1key = new TrigConfL1PsKeyNamed(this.partition, "RunParams.TrigConfL1PsKey");
            Repository isRepository = new Repository(this.m_partition);
            //isRepository.getValue("RunParams.TrigConfL1PsKey", isl1key);
            // New place to look for this value
            // Loaded, or scheduled?
            isRepository.getValue("L1CT.CtpControllerLoadedL1PSKey", isl1key);
            return isl1key.L1PrescaleKey;
        }
        catch(is.InfoNotFoundException|is.RepositoryNotFoundException|is.InfoNotCompatibleException ex) {
            Logger.errorErs(new APELogger.ISIssue("Did not find RunParams.TrigConfL1PsKey. Assume it is missing. " + ex.toString()));
            Logger.logStacktrace(ex);
        }
        return 0;
    }


    /**
     * Read SMK from the IS: RunParams.TrigConfSmKey
     *
     *  @return Read smk
     */
    public int getSMKFromIS() throws InterruptedException
    {
        Repository isRepository = new Repository(this.m_partition);

        TrigConfSmKeyNamed issmk = new TrigConfSmKeyNamed(this.m_partition, "RunParams.TrigConfSmKey");
        while(true) {
            try {
                isRepository.getValue("RunParams.TrigConfSmKey", issmk);
                return issmk.SuperMasterKey;
            }
            catch (is.InfoNotFoundException|is.RepositoryNotFoundException|is.InfoNotCompatibleException e) {
                Logger.errorErs(new APELogger.ISIssue("Did not find RunParams.TrigConfSmKey. " + e.toString()));
                Logger.logStacktrace(e);
                Object o = new Object();
                synchronized (o) {
                    o.wait(10*1000);
                }
            }
        }
    }

}