package autoprescaleeditor;

import java.util.Vector;
import ipc.Partition;
import TTCInfo.AutoPrescaleCTPNamed;
import TTCInfo.AutoPrescaleCTP;

import org.apache.commons.cli.*;

// Used in test/listenIS
public class L1PssISListener {

    public static void main( String[] args ) {

        String partition = "ATLAS";
        String isname    = "RunParams.L1PrescaleRequest";
        int    smk       = 0;

		CommandLine cmd = null;
		Options options = new Options();
		try {
		    CommandLineParser parser = new DefaultParser(); //PosixParser();
			options.addOption("h", false, "help");
			options.addOption("p", true, "partition name");
			options.addOption("n", true, "is object name");
			options.addOption("s", true, "smk");
			cmd = parser.parse(options, args);
		} catch (ParseException ex) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("SendL1Pss2IS <options>", options);
			System.exit(1);
		}


		if (cmd.hasOption("h")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("SendL1Pss2IS <options>", options);
			System.exit(0);
		}
		if (cmd.hasOption("p")) partition = cmd.getOptionValue("p");
		if (cmd.hasOption("n")) isname    = cmd.getOptionValue("n");
		if (cmd.hasOption("s")) smk       = Integer.valueOf(cmd.getOptionValue("s"));

        L1PssISListener pss_listener = new L1PssISListener(partition,isname);
        for(;;) {;}
    }



    private int current_psk = 0;
    private long[] l1_prescales = null;
    private APEAbsISListener<AutoPrescaleCTP> listener = null;

    public L1PssISListener(final String partition,
                           final String isname)
    {
        this.listener = new APEAbsISListener<AutoPrescaleCTP>
            (new ipc.Partition(partition),
             isname,
             AutoPrescaleCTP.class,
             new UpdateInfoT<AutoPrescaleCTP>() {
                @Override
                    public void updateInfo(final AutoPrescaleCTP info) {
                    current_psk = info.L1_PSK_current;
                    l1_prescales = info.L1_Prescales;
                    System.out.println("AutoPrescale request received");
                }
            });

        listener.subscribe();
        System.out.println("Subscribed to " + partition + "." + isname);
    }

    public int get_current_ls_psk() { return current_psk; }

    public long[] get_l1_prescales() { return l1_prescales; }

}
