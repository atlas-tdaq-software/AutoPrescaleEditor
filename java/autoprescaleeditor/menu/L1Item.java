package autoprescaleeditor.menu;


// Class representing L1Item from the L1Menu
public class L1Item {

    // Item's name
    private String m_name = "";

    // CTPID of item
    private int m_ctpId = -1;

    public L1Item () {}

    public L1Item (String name, int ctpId) {
        this.m_name = name;
        this.m_ctpId = ctpId;
    }

    public String getName () {return this.m_name;}

    public int getCtpId () {return this.m_ctpId;}
}