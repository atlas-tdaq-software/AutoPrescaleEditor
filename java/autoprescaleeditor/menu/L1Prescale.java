package autoprescaleeditor.menu;

import java.util.Map;
import java.util.HashMap;
import java.io.FileReader;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.lang.reflect.Type;

import java.util.Date;
import java.text.SimpleDateFormat;

import com.google.gson.Gson;
import com.google.gson.*;

import autoprescaleeditor.APELogger;
import autoprescaleeditor.AutoPrescaleEditor;


// Wrapper for L1 Prescale Set JSON
public class L1Prescale {

    // Maximum number of ctpids
    public static int MAX_LENGTH = 512;

    // Logger
    private static final APELogger Logger = APELogger.createLogger("L1Prescale");

    // Object to store the JSON dictionary with prescales set
    private JsonObject m_prescalesJson;

    // Comment about the Prescales set
    // Not part of L1Prescales JSON, to be saved in the upload command
    private String m_comment;

    // Filename to read or save the prescales set
    private String m_filename;

    // L1 Prescales set database key
    private int m_key;

    // Map from item to ctp id
    private Map<Integer,String> m_ctpIdToItemNameMap = new HashMap<Integer,String>();

    /**
     *  Calculate the cut from prescale value
     *  Function imported from TriggerTool
     *
     * @param frequency: Prescale value
     *
     * @return cut value
     **/
    public static int calculateCutFromPrescale(double frequency) {
        // C = 2**24-(2**24-1)/PS

        int sign = frequency>=0 ? 1 : -1;
        
        double cut =  0x1000000 - 0xFFFFFF/Math.abs(frequency);
        cut = Math.round(cut);
        if(cut==0x1000000) {
            cut = 0xFFFFFF; // for prescale values that are larger than 2*24-1
        }
        
        return (int)(sign*cut);
    }

    /**
     *  Calculate the prescale from the cut value
     *  Function imported from TriggerTool
     *
     * @param dbValue: Cut value
     *
     * @return prescale value
     **/
    public static BigDecimal calculatePrescaleFromCut(int dbValue)
    {
        // PS = (2**24-1)/(2**24-C)
        double sign = dbValue>=0 ? 1 : -1;
        int validCut = Math.abs(dbValue);
        if(validCut==0) {
            validCut = 1;
        }
        if(validCut>0xFFFFFF) {
            validCut=0xFFFFFF;
        }
        
        return new BigDecimal((sign * 0xFFFFFF) / (0x1000000 - validCut));
    }


    /**
     *  Validate cut value based on TrigDb/L1Prescale.java#L164
     *
     * @param newCut: Cut value to validate
     *
     * return validated cut value
     **/
    public static int validateCutFromCTP(int newCut){
        if (newCut < 0) {
            Logger.debug("Cut requested for disabled item!");
            return newCut;
        }

        // Cut received from ctp is 1 less than TriggerDB cut value ATR-24392
        int convertedCutValue = (newCut == 1) ? 1 : newCut + 1;
        
        if (convertedCutValue > 0xFFFFFF) {
            Logger.debug("Cut larger than 0xFFFFFF :" + convertedCutValue + ", adjusted to maximum");
            return 0xFFFFFF;
        }

        return convertedCutValue;
    }


    // Default constructor
    public L1Prescale () {
        // Print debug that configure from file and map has to be set
    }

    public L1Prescale (String filename, Map<Integer, String> m) {
        configureFromFile(filename);
        setCtpIdToItemNameMap(m);
    }

    // Copy constructor
    public L1Prescale (L1Prescale other) {
        this.m_prescalesJson = other.m_prescalesJson.deepCopy();
        this.m_comment = other.m_comment;
        this.m_filename = other.m_filename;
        this.m_key = other.m_key;
        this.m_ctpIdToItemNameMap = other.m_ctpIdToItemNameMap;
    }

    /**
     *  Validate the L1Prescale object - if it contains valid prescales dictionary
     *  and item name to id map
     *
     * @return Boolean if object valid
     **/
    protected Boolean validateObject() {
        if (m_ctpIdToItemNameMap == null || m_ctpIdToItemNameMap.isEmpty()){
            Logger.errorErs(new APELogger.ConfigIssue("Map item name to ctp id is not available! Make sure to call setCtpIdToItemNameMap for this object."));
            return false;
        }

        if (m_prescalesJson == null){
            Logger.errorErs(new APELogger.ConfigIssue("L1 prescales set not available! Make sure to call configureFromFile for this object."));
            return false;
        }

        return true;
    }


    /**
     *  Configure the L1Prescale object from file
     *
     * @param filename: Name of the file to read
     **/
    private void configureFromFile (String filename) {
        Logger.info("Configuring the L1Prescale Set from file " + filename);
        try {
            Gson gson = new Gson();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            this.m_prescalesJson = gson.fromJson(bufferedReader, JsonObject.class);
            bufferedReader.close();

            this.m_filename = filename;
        } catch (IOException e) {
            Logger.fatalErs(new APELogger.IOIssue("Reading L1 Prescale set from file " + filename + " failed "));
            Logger.logStacktrace(e);
        }

        Logger.debug("Reading from file success");
    }

    /**
     *  Save the L1Prescale Set dictionary to file
     *
     * @return Name of the file that the set was saved to
     **/
    public String saveToFile () {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
        String newFilename = AutoPrescaleEditor.OutputDir + "AUTO_" 
                    + this.m_filename.replace(AutoPrescaleEditor.OutputDir, "").replace(".json", "")  
                    + "_" + timeStamp + ".json";
        Logger.info("The prescales will be saved to "  + newFilename);

        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            OutputStream outputStream = new FileOutputStream(newFilename);
            outputStream.write(gson.toJson(this.m_prescalesJson).getBytes());
            outputStream.flush();

        } catch (FileNotFoundException e) {
            Logger.errorErs(new APELogger.IOIssue("File to save " + newFilename + " not found."));
            Logger.logStacktrace(e);
            return "";

        } catch (IOException e) {
            Logger.errorErs(new APELogger.IOIssue("Saving to file " + newFilename + " failed." ));
            Logger.logStacktrace(e);
            return "";
        }

        Logger.debug("Saving to file success");

        return newFilename;
    }

    // Default setters and getters
    public void setCtpIdToItemNameMap ( Map<Integer, String> m ) {m_ctpIdToItemNameMap = m;}

    public String getName () { return this.m_prescalesJson.get("name").getAsString(); }

    public void setName (String name) {this.m_prescalesJson.addProperty("name", name);}

    public String getComment () {return this.m_comment;}

    public void setComment (String comment) {
        if (comment.length() > 200) { //TriggerDB limit
            this.m_comment = (new StringBuilder(comment.substring(0, 194)).append("...")).toString();
        } else {
            this.m_comment = comment;
        }
    }

    public int getId() {return this.m_key;}

    public void setId(int key) {this.m_key = key;}


    /**
     *  Read the cut value for an item
     *
     * @param ctpId: CTPID of the item
     *
     * @return Cut value or -1 if object not valid
     **/
    public int getCutValue (int ctpId) {
        if (!validateObject()) {return -1;}

        String name = m_ctpIdToItemNameMap.get(ctpId);
        //IguiLogger.info("Found " + name + " for ctpid " + ctpId);

        if (name == null){
            Logger.errorErs(new APELogger.Issue("Item name for ctp ID " + ctpId + " not found!"));
            return -1;
        }
        return getCutValue(m_ctpIdToItemNameMap.get(ctpId)); 
    }

    /**
     *  Read the cut value for an item
     *
     * @param itemName: Name of the item
     *
     * @return Cut value or -1 if object not valid or exception happened
     **/
    public int getCutValue (String itemName) {
        if (!validateObject()) {return -1;}
        try {
            int cutVal = this.m_prescalesJson.getAsJsonObject("cutValues").getAsJsonObject(itemName)
                                .get("cut").getAsInt();
            return cutVal;
        } catch (Exception e) {
            Logger.errorErs(new APELogger.IOIssue("Error when reading cut from L1 Prescale set for item " + itemName));
            return -1;
        }
    }

    /**
     *  Set the cut value and prescale for an item
     *
     * @param ctpId: CTPID of the item
     * @param newCutValue: New cut value to set
     **/
    public void setCutValue (int ctpId, int newCutValue) { 
        if (!validateObject()) {return;}
        String name = m_ctpIdToItemNameMap.get(ctpId);

        if (name == null){
            Logger.errorErs(new APELogger.Issue("Item name for ctp ID " + ctpId + " not found!"));
            return;
        }

        setCutValue(m_ctpIdToItemNameMap.get(ctpId), newCutValue); 
    }

    /**
     *  Set the cut value and prescale for an item
     *
     * @param itemName: Name of the item
     * @param newCutValue: New cut value to set
     **/
    public void setCutValue (String itemName, int newCutValue) {
        if (!validateObject()) {return;}

        Logger.debug("Setting cut value " + newCutValue + " for item name " + itemName);
        this.m_prescalesJson.getAsJsonObject("cutValues").getAsJsonObject(itemName)
                        .addProperty("cut", newCutValue);

        // Prescale value is rounded to be in agreement with the RuleBook ATR-26932
        BigDecimal prescale = L1Prescale.calculatePrescaleFromCut(newCutValue).setScale(8, RoundingMode.HALF_UP);
        this.m_prescalesJson.getAsJsonObject("cutValues").getAsJsonObject(itemName)
                        .addProperty("info", "prescale: " + prescale.toPlainString());

        Logger.debug("Setting the prescale to " + prescale.toPlainString());
    }


    /**
     *  Read the prescale value for an item
     *
     * @param ctpId: CTPID of the item
     *
     * @return Prescale value or -1 if object not valid
     **/
    public Double getPrescale (int ctpId) {
        if (!validateObject()) {return -1.0;}

        String name = m_ctpIdToItemNameMap.get(ctpId);

        if (name == null){
            Logger.errorErs(new APELogger.Issue("Item name for ctp ID " + ctpId + " not found!"));
            return -1.0;
        }
        return getPrescale(m_ctpIdToItemNameMap.get(ctpId)); 
    }


    /**
     *  Read the prescale value for an item
     *
     * @param itemName: Name of the item
     *
     * @return Prescale value or -1 if object not valid or exception happened
     **/
    public Double getPrescale (String itemName) {
        if (!validateObject()) {return -1.0;}
        try {
            String infoStr = this.m_prescalesJson.getAsJsonObject("cutValues").getAsJsonObject(itemName)
                                .get("info").getAsString();
            return Double.parseDouble(infoStr.replace("prescale: ", ""));
        } catch (Exception e) {
            Logger.errorErs(new APELogger.IOIssue("Error when reading prescale from l1 prescale set for item " + itemName));
            return -1.0;
        }
    }

    /**
     *  Read the item name for given ctpid
     *
     * @param ctpId: CTPID of the item
     *
     * @return Name or "" if object not valid or exception happened
     **/
    public String getNameForCtpId (int ctpId) {
        if (!validateObject()) {return "";}

        String name = m_ctpIdToItemNameMap.get(ctpId);

        if (name == null){
            Logger.errorErs(new APELogger.Issue("Item name for ctp ID " + ctpId + " not found!"));
            return "";
        }

        return name;
    }

    /**
     *  Return string report of the prescales set
     *
     * @return report
     **/
    // public String prescaleReport() {
    //     String report = "";
    //     for (Map.Entry<String, JsonElement> entry : this.m_prescalesJson.getAsJsonObject("cutValues").entrySet()) {

    //         JsonObject entryObj = entry.getValue().getAsJsonObject();

    //         report += "Item " + entry.getKey() + " cut " + entryObj.get("cut").getAsString() + " " + entryObj.get("info").getAsString();
    //     }

    //     return report;
    // }
}
