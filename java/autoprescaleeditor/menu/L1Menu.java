package autoprescaleeditor.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.*;

import autoprescaleeditor.menu.L1Item;
import autoprescaleeditor.APELogger;


// Wrapper for L1 Menu JSON
public class L1Menu {

    // Logger
    private static final APELogger Logger = APELogger.createLogger("L1Menu");

    // Object to store the JSON dictionary with the menu
    private JsonObject m_l1MenuJson = null;

    // Cached L1 items list
    private List<L1Item> m_itemsList = null;


    public L1Menu (String filename) {
        configureFromFile(filename);
    }

    /**
     *  Configure the L1Menu object from file
     *
     * @param filename: Name of the file to read
     **/
    private void configureFromFile (String filename) {
        Logger.info("Configuring L1Menu from file " + filename); 
        try{
            Gson gson = new Gson();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            this.m_l1MenuJson = gson.fromJson(bufferedReader, JsonObject.class);
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            Logger.fatalErs(new APELogger.IOIssue("L1 Menu input file " + filename + " not found: ")); 
            Logger.logStacktrace(e);
        } catch (IOException e) {
            Logger.fatalErs(new APELogger.IOIssue("Reading L1 Menu from file " + filename + " failed: ")); 
            Logger.logStacktrace(e);
        }

        // Reset the cached L1 items list
        m_itemsList = new ArrayList<L1Item>();

        for (Map.Entry<String, JsonElement> entry : this.m_l1MenuJson.getAsJsonObject("items").entrySet()) {
            JsonObject itemObj = entry.getValue().getAsJsonObject();
            L1Item i = new L1Item(itemObj.get("name").getAsString(), itemObj.get("ctpid").getAsInt());
            m_itemsList.add(i);
        }
    }

    /**
     *  Create and return the list of L1 items wrapped in L1Item objects
     *      read from the JSON dictionary
     *
     * @return list of L1Items
     **/
    public final List<L1Item> getItems() {
        if (m_itemsList != null) return m_itemsList;
        else {
            Logger.warningErs(new APELogger.Issue("The list of L1 items is not available - the L1 Menu was not loaded")); 
            return new ArrayList<L1Item>();
        }
    }
}
