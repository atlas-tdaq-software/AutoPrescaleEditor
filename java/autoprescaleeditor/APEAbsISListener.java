package autoprescaleeditor;

import is.InfoListener;
import is.Repository;
import is.Info;
import is.InfoEvent;

import is.RepositoryNotFoundException;
import is.AlreadySubscribedException;
import is.InfoNotFoundException;
import is.SubscriptionNotFoundException;

/**
 * Abstract IS listener
 *
 * @author Joerg Stelzer
 */
public class APEAbsISListener<T extends is.Info> implements InfoListener {

    // Logger
    private static final APELogger Logger = APELogger.createLogger("APEAbsISListener");

    private final Class<T> infoType;

    /** IS info name: server.provider */
    protected final String infoname;  // e.g. 'RunParams.RunParams', or 'RunParams.Ready4Physics', or 'RunParams.LuminosityInfo'

    /** IS repository */
    private final Repository isRepository;

    /** Are we subscribed? */
    private boolean subscribed;

    /** update function pointer */
    private final UpdateInfoT<T> updateinterface;
    
    /**
     * Constructor.
     * @param partition
     * @param infoname
     * @param infoType
     * @param update
     */
    public APEAbsISListener(final ipc.Partition partition,
                            final String infoname, 
                            final Class<T> infoType,
                            final UpdateInfoT<T> update) {

        this.infoType          = infoType; // the class type
        this.infoname          = infoname; // is info name
        this.subscribed        = false;
        this.updateinterface   = update;
        this.isRepository      = new Repository(partition);
    }

    /**
     * Subscribe to the IS server
     */
    public void subscribe() {
        
        try {
            if(!subscribed) {
                isRepository.subscribe(infoname,this);
                subscribed = true;
            }
            // immediately update with current state
            // updateinterface.updateInfo(getinfo());
        }
        catch(RepositoryNotFoundException ex) {
            //trigPanel.getIgui().internalMessage(IguiConstants.MessageSeverity.ERROR,"RepositoryNotFoundException when subscribing to IS service " + infoname);
            subscribed = false;
        }
	catch(AlreadySubscribedException ex) {
	    //trigPanel.getIgui().internalMessage(IguiConstants.MessageSeverity.ERROR,"InvalidCriteriaException when subscribing to IS service " + infoname);
	    subscribed = false;
	}

    }//subscribe()

    /**
     * Un-subscribe to the IS server
     */
    public void unsubscribe() {
        try{
            isRepository.unsubscribe(infoname);
            //IguiLogger.info("Unsubscribing to IS service RunParams with subscription LBSettings");
            subscribed=false;
        }
        catch(RepositoryNotFoundException ex) {
            Logger.warningErs(new APELogger.Issue("RepositoryNotFoundException when unsubscribing to IS service " + infoname));
        }
        catch(SubscriptionNotFoundException ex) {
            //trigPanel.getIgui().internalMessage(IguiConstants.MessageSeverity.ERROR,"SubscriptionNotFoundException when unsubscribing to IS service RunParams with subscription LBSettings");
        }
    }


    /**
     * Function to get the current settings from the IS repository
     */
    public final T getinfo() {
        
        T isInfo = null;

        try{
            isInfo = this.getInfoInstance();
            isRepository.getValue(infoname, isInfo);
        }
        catch (RepositoryNotFoundException ex) {
            Logger.warningErs(new APELogger.ISIssue("RepositoryNotFoundException: " + infoname));
            return null;
        }
        catch (InfoNotFoundException ex) {
            Logger.warningErs(new APELogger.ISIssue("InfoNotFoundException: " + infoname));
            return null;
        }
        catch(final Exception ex) {
            Logger.warningErs(new APELogger.ISIssue("Failed to get info \"" + infoname + "\": " + ex.toString()));
            Logger.logStacktrace(ex);
        }

        return isInfo;
    }

    public void infoCreated(InfoEvent infoevent) {
        try {
            final T isInfo = this.getInfoInstance();
            infoevent.getValue(isInfo);
            updateinterface.updateInfo(isInfo);
        }
        catch(final Exception ex) {
            final String msg = "Failed to update info \"" + infoevent.getName() + "\": " + ex
                + ". Information may not be correctly updated";
            Logger.errorErs(new APELogger.ISIssue(msg));
            Logger.logStacktrace(ex);
        }
    }

    public void infoUpdated(InfoEvent infoevent) {
        try {
            final T isInfo = this.getInfoInstance();
            infoevent.getValue(isInfo);
            updateinterface.updateInfo(isInfo);
        }
        catch(final Exception ex) {
            final String msg = "Failed to update info \"" + infoevent.getName() + "\": " + ex
                + ". Information may not be correctly updated";
            Logger.errorErs(new APELogger.ISIssue(msg));
            Logger.logStacktrace(ex);
        }
    }

    public void infoDeleted(InfoEvent arg0) {
    }

    private T getInfoInstance() {
        try{
            return this.infoType.newInstance();
        }
        catch(final Exception ex) {
            final String msg = "Failed to create new Instance: " + ex;
            Logger.errorErs(new APELogger.ISIssue(msg));
            Logger.logStacktrace(ex);
        }
        return null;
    }
}
