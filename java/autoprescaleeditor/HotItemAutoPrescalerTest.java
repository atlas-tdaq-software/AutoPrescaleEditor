package autoprescaleeditor;

import autoprescaleeditor.L1PrescaleSaver.RequestState;
import autoprescaleeditor.L1PrescaleSaver.ValidationResult;
import java.sql.SQLException;
import autoprescaleeditor.menu.L1Prescale;

import java.util.Vector;


public class HotItemAutoPrescalerTest {

    public static void main( String[] args ) throws SQLException {

        final int smk = 6;
        final int orig_l1_psk = 5;
        int[] new_pss_int = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1,
                                        1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, -1, 1,
                                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1,
                                        1, 1, 1, 1, 1, 1, 1, -1, 1, 1, -1, -1, 1, 1, 1, 1, 1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1,
                                        -1, 1, 1, -1, -1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                        1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

        new_pss_int[152] = 15;  // L1_TAU5_EMPTY
        new_pss_int[168] = 7;   // L1_J5_EMPTY
        new_pss_int[177] = 300; // L1_EM3_EMPTY

        Vector<String> items_allowed_to_change = new Vector<String>();
        items_allowed_to_change.add("L1_EM3_EMPTY");
        items_allowed_to_change.add("L1_TAU5_EMPTY");
        items_allowed_to_change.add("L1_J5_EMPTY");

        L1PrescaleSaver prescaleSaver = new L1PrescaleSaver("TRIGGERDBJOERG", items_allowed_to_change);

        L1Prescale new_pss = new L1Prescale(); 
        int ctpid = 0;
        for( int ps: new_pss_int ) { new_pss.setCutValue(ctpid++,ps); }
        // This test will not work now, if L1PrescaleSaver.readPss is not called, the map ctp to name will not be set

        // at the begin of run this should be done
        prescaleSaver.configureWithSMK(smk);

        // whenever a new prescale set is requested
        int new_l1_psk = -1;

        // whenever a new prescale set is requested
        ValidationResult request = prescaleSaver.validateNewPrescales(new_pss, new_pss);
        if( request.getState() == RequestState.VALID ) { 
            new_l1_psk = prescaleSaver.saveNewL1PrescaleSet(new_pss);
        } else {
            if( request.getState() != RequestState.CANCELED_REQUEST )
                System.err.println("Not a valid request. Won't upload new LVL1 prescale set");
        }


        // confirmation message
        if( new_l1_psk > 0 ) {
            System.out.println("New l1 prescale key has been created: "+new_l1_psk);
        } else {
            System.out.println("Failed to upload a new L1 prescale set.");
        }
    }

}
