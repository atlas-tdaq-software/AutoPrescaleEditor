package autoprescaleeditor;

import ers.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.*;
import java.sql.SQLException;
import java.util.*;
import autoprescaleeditor.menu.L1Prescale;
import TTCInfo.TrigConfSmKeyNamed;
import TTCInfo.AutoPrescaleCTPNamed;

import java.util.List;
import java.util.Map;
import org.apache.commons.cli.*;

@SuppressWarnings("serial")
class TestSendIssue extends ers.Issue {
    TestSendIssue(final String message) { super(message); }
}
@SuppressWarnings("serial")
class TestISIssue extends ers.Issue {
    TestISIssue(final String message) { super(message); }
}
@SuppressWarnings("serial")
class TestConfigIssue extends ers.Issue {
    TestConfigIssue(final String message) { super(message); }
}

public class TestAutoPrescaling {

    private enum READYSTATE
    { STANDBY, PHYSICS, UNKNOWN }

    static void info   (final ers.Issue i) { Logger.info(i); }
    static void warning(final ers.Issue i) { Logger.warning(i); }
    static void error  (final ers.Issue i) { Logger.error(i); }
    static void fatal  (final ers.Issue i) { Logger.fatal(i); }
    static void debug  (final ers.Issue i, final int n) {Logger.debug(n, i); }
    static void log    (final ers.Issue i) { Logger.error(i); }

    // elog pw from file
    private ELog elog=null;


    private is.Repository isRepository;
    private AutoPrescaleCTPNamed ctpInfo;
    final ipc.Partition thePartition;
    final String triggerdb;
    final String isname;
    final String configFile;
    final String elogpwfile;

    public TestAutoPrescaling( final String partition, 
                               final String isname,
                               final String triggerdb,
                               final String configfile,
                               final String elogpwfile )
    {
        this.thePartition = new ipc.Partition(partition);
        this.isRepository = new is.Repository(thePartition);
        this.isname       = isname;
        this.triggerdb    = triggerdb;
        this.configFile   = configfile;
        this.ctpInfo      = new AutoPrescaleCTPNamed(thePartition, this.isname);
        this.elogpwfile   = elogpwfile;

    }
    
    
    void reset()
    {
        ctpInfo.L1_PSK_reset = 1; // anything not null with issue a reset
        String msg = "Requesting reset to default prescale set ";
        System.out.println(msg);
        info(new TestSendIssue(msg));
        try {
            ctpInfo.checkin();
        }
        catch(Exception ex) {
           System.out.println("Exception: "+ex.getMessage());
           ex.printStackTrace();
        }
    }
    
    
    private int readInt(final String prompt)
    {
        System.out.print(prompt);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String inputString = bufferedReader.readLine();
            return Integer.parseInt(inputString);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (NumberFormatException e) {
            System.out.print("Wrong input!");
            return -1;
        }
        return 0;
    }
    

    private final String readString(final String prompt)
    {
        System.out.print(prompt);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String inputString = bufferedReader.readLine();
            return inputString;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (NumberFormatException e) {
            System.out.print("Wrong input!");
            return "";
        }
        return "";
    }
    
    
    boolean isUpdate()
    {
        getSMKFromIS();
        getL1PSKFromIS();
        READYSTATE rs = getCurrentStateFromIS();
        int standbyKey = getL1KeyFromIS(READYSTATE.STANDBY);
        int physicsKey = getL1KeyFromIS(READYSTATE.PHYSICS);
        
        String msg = "Currently in IS\n";
        msg += "1) SMK           : " + ctpInfo.SMK_current + "\n";
        msg += "2) L1 PSK (CTP)  : " + ctpInfo.L1_PSK_current + "\n";
        msg += "3) State         : " + rs.toString() + "\n";
        msg += "4) STANDBY L1 PSK: " + standbyKey + "\n";
        msg += "5) PHYSICS L1 PSK: " + physicsKey + "\n";
        System.out.println(msg);
        
        int change = readInt("Update which (0-exit): ");
        if(change==0) { return false; }
        switch(change) {
        case 0: return false;
        case 1:
            int smk = readInt("New SMK (0-cancel): ");
            if(smk==0) { break; }
            sendSMKToIS(smk);
            break;
        case 2:
            int psk = readInt("New Current key (0-cancel): ");
            if(psk==0) { break; }
            sendL1PSKToIS(psk);
            break;
        case 3: 
            int state = readInt("New State (0-cancel, 1-standby, 2-physics): ");
            if(state==0) { break; }
            sendCurrentStateToIS(state==1?READYSTATE.STANDBY:READYSTATE.PHYSICS);
            break;
        case 4:
            int spsk = readInt("New STANDBY key (0-cancel): ");
            if(spsk==0) { break; }
            sendL1KeyToIS(READYSTATE.STANDBY, spsk);
            break;
        case 5:
            int ppsk = readInt("New PHYSICS key (0-cancel): ");
            if(ppsk==0) { break; }
            sendL1KeyToIS(READYSTATE.PHYSICS, ppsk);
            break;
        }
        return true;
    }

    final List<Integer> prescaleFactors() {
        List<Integer> items = new ArrayList<Integer>();
        try {
            File f = new File("prescaleFactors.txt");
            BufferedReader reader = new BufferedReader(new FileReader(f));
            String line = null;
            while ((line=reader.readLine()) != null) {
                Scanner linescanner = new Scanner(line);
                try {
                    items.add(Integer.valueOf(linescanner.next()));
                }
                catch(Exception ex) {}
            }
            reader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Did not find file prescaleFactors.txt");
        }
        catch (IOException e) {
            System.out.println("Could not read file prescaleFactors.txt");
        }
        return items;
    }

    
    void sendNewPSS(final boolean execute) throws SQLException
    {
        if(!getSMKFromIS()) { return; }
        if(!getL1PSKFromIS()) { return; }
        
        List<Integer> prescale_factors = prescaleFactors();
        
        List<String> items_allowed_to_change = AutoPrescaleEditor.extractItemNames(this.configFile);

        // the prescale saver is just use to get the menu
        L1PrescaleSaver prescaleSaver = new L1PrescaleSaver(triggerdb, items_allowed_to_change, false);
        prescaleSaver.configureWithSMK(ctpInfo.SMK_current);
        
        L1Prescale l1ps = prescaleSaver.readL1PSS(ctpInfo.L1_PSK_current);

        Map<Integer, String> itemNames = prescaleSaver.getItemNameByCtpId();

        int curPrescaleFactorListIdx = 0;
    
        // create our own PS set to update
        ctpInfo.L1_Prescales = new long[256];
        for(int i=0; i<256; i++) {
            int ps = l1ps.getCutValue(i+1);
            if(ps>0) {
                String itemName = itemNames.get(i);
                if( items_allowed_to_change.contains(itemName) ) {
                    int psfactor = curPrescaleFactorListIdx<prescale_factors.size()?prescale_factors.get(curPrescaleFactorListIdx++):12;
                    int newps = ps * psfactor;
                    System.out.println("Changing prescale of " + itemName + " from " + ps + " to " + newps);
                    ps = newps;
                }
            }
            this.ctpInfo.L1_Prescales[i] = ps;
        }

        if(execute) {
            try {
                info(new TestSendIssue("Sending new test prescale set to IS with SMK=" + ctpInfo.SMK_current + ", \n based on current L1 PSK: " + ctpInfo.L1_PSK_current));
                ctpInfo.checkin();
            }
            catch(Exception ex) {
                System.out.println("Exception: "+ex.getMessage());
                ex.printStackTrace();
            }
            System.out.println("DONE SENDING");
        }
    }
        

    void sendSMKToIS(final int smk)
    {
        TrigConfSmKeyNamed issmk = new TrigConfSmKeyNamed(thePartition, "RunParams.TrigConfSmKey");
        issmk.SuperMasterKey = smk;
        try {
            isRepository.update("RunParams.TrigConfSmKey", issmk);
        }
        catch(is.InfoNotFoundException e) {
            isRepository.insert("RunParams.TrigConfSmKey", issmk);
        }
        return;
    }
    
    
    boolean getSMKFromIS()
    {
        TrigConfSmKeyNamed issmk = new TrigConfSmKeyNamed(thePartition, "RunParams.TrigConfSmKey");
        try {
            isRepository.getValue("RunParams.TrigConfSmKey", issmk);
            ctpInfo.SMK_current = issmk.SuperMasterKey;
            return true;
        }
        catch(is.InfoNotFoundException e) {
            String msg = "Did not find RunParams.TrigConfSmKey." + e.toString();
            System.out.println(msg);
            error(new TestISIssue( msg));
        }
        ctpInfo.SMK_current = -1;
        return false;
    }

    
    private void sendL1PSKToIS(final int key)
    {
        final String infoname = "RunParams.TrigConfL1PsKey";
        TrigConfL1PsKeyNamed isl1key = new TrigConfL1PsKeyNamed(this.thePartition, infoname);
        isl1key.L1PrescaleKey = key;
        try {
            isRepository.checkin( infoname, isl1key);
        }
        catch(is.InfoNotFoundException e) {
            isRepository.insert( infoname, isl1key);
        }
        return;
    }


    boolean getL1PSKFromIS()
    {
        TrigConfL1PsKeyNamed isl1psk = new TrigConfL1PsKeyNamed(thePartition, "RunParams.TrigConfL1PsKey");
        try {
            isRepository.getValue("RunParams.TrigConfL1PsKey", isl1psk);
            ctpInfo.L1_PSK_current = isl1psk.L1PrescaleKey;
            return true;
        }
        catch(is.InfoNotFoundException e) {
            String msg = "Did not find IS info RunParams.TrigConfL1PsKey.";
            System.out.println(msg);
            error(new TestISIssue( msg));
        }
        catch(Exception e) {
            System.out.println(e); 
            error(new TestISIssue( e.toString()));
            e.printStackTrace();
        }
        ctpInfo.L1_PSK_current = -1;
        return false;
    }

    
    private class BooleanInfo extends is.NamedInfo
    {
        boolean Ready4Physics;
        public BooleanInfo( ipc.Partition partition, String name ) {
            super( partition, name, "Boolean" );
        }
        @Override
        public void publishGuts( is.Ostream out ){
            super.publishGuts( out );
            out.put( Ready4Physics );
        }
        @Override
        public void refreshGuts( is.Istream in ){
            super.refreshGuts( in );
            Ready4Physics = in.getBoolean();
        }
    }


    private void sendCurrentStateToIS(final READYSTATE rs)
    {
        String infoname = "RunParams.Ready4Physics";
        BooleanInfo bi = new BooleanInfo(this.thePartition, infoname);
        bi.Ready4Physics = (rs==READYSTATE.PHYSICS);
        try {
            isRepository.checkin( infoname, bi);
        }
        catch(is.InfoNotFoundException e) {
            isRepository.insert( infoname, bi);
        }
        return;
    }
    
    private READYSTATE getCurrentStateFromIS()
    {
        BooleanInfo bi = new BooleanInfo(this.thePartition, "RunParams.Ready4Physics");
        try {
            isRepository.getValue("RunParams.Ready4Physics", bi);
            Boolean physics = bi.Ready4Physics;
            return physics?READYSTATE.PHYSICS:READYSTATE.STANDBY;
        }
        catch(is.InfoNotFoundException ex) {
            String msg = "Did not find IS info RunParams.Ready4Physics.";
            System.err.println(msg);
            error(new TestISIssue( msg ));
        }
        return READYSTATE.UNKNOWN;
    }
    
    
    private void sendL1KeyToIS(final READYSTATE rs, final int key)
    {
        if(rs==READYSTATE.UNKNOWN) { return; }
        final String infoname = "RunParams."+(rs==READYSTATE.PHYSICS?"Physics":"Standby")+".L1PsKey";
        TrigConfL1PsKeyNamed isl1key = new TrigConfL1PsKeyNamed(this.thePartition, infoname);
        isl1key.L1PrescaleKey = key;
        try {
            isRepository.checkin( infoname, isl1key);
        }
        catch(is.InfoNotFoundException e) {
            isRepository.insert( infoname, isl1key);
        }
        return;
    }

    
    private int getL1KeyFromIS(final READYSTATE rs)
    {
        if(rs==READYSTATE.UNKNOWN) { return 0; }

        final String _isname = "RunParams."+(rs==READYSTATE.PHYSICS?"Physics":"Standby")+".L1PsKey";
        TrigConfL1PsKeyNamed isl1key = new TrigConfL1PsKeyNamed(this.thePartition, _isname);
        try {
            isRepository.getValue(_isname, isl1key);
            int l1_key = isl1key.L1PrescaleKey;
            return l1_key;
        }
        catch(is.InfoNotFoundException ex) {
            String msg = "Did not find IS info " + _isname + ".";
            System.err.println(msg);
            error(new TestISIssue(msg));
        }
        return -1;
    }
    
    private void block() {
        
    }
    
    private void resume() {
        
    }

    private void sendElogMsg() {
        if(this.elog == null) { this.elog = new ELog(elogpwfile); }
        final String msg  = readString("Elog message: ");
        //elog.sendElogMsg("", msg);
    }

    public static void main( String[] args ) throws SQLException
    {

        String partition  = "ATLAS";
        String isname     = "L1CT.AutoPrescaleCTPTest";
        String triggerdb  = "TRIGGERDB";
        String configFile = "/det/ctp/MaxItemRates.txt";
        String elogpwfile = "/atlas-home/1/stelzer/APE/.elogpw.txt";

        CommandLine cmd = null;
        Options options = new Options();
        try {
            CommandLineParser parser = new PosixParser();
            options.addOption("h",  false, "help");
            options.addOption("p",  true, "partition name ["+partition+"]");
            options.addOption("n",  true, "is object name ["+isname+"]");
            options.addOption("db", true, "triggerdb ["+triggerdb+"]");
            options.addOption("reset",  false, "reset to l1 psk [false]");
            options.addOption("isupdate", false, "IS update [false]");
            options.addOption("block", false, "block");
            options.addOption("resume", false, "resume");
            options.addOption("c", true, "Config file ["+configFile+"]");
            options.addOption("elog", true, "elog pw file [default="+elogpwfile+"]");
            options.addOption("sendps", true, "sends presales, [args=show|exec]");
            cmd = parser.parse(options, args);
        } catch (ParseException ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(ex.toString() + "\nautoprescaleeditor.TestAutoPrescaling <options>", options);
            System.exit(1);
        }
        if (cmd.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("autoprescaleeditor.TestAutoPrescaling <options>", options);
            System.exit(0);
        }
        try {
            partition = cmd.getOptionValue("p");
            isname    = cmd.getOptionValue("n");
            triggerdb = cmd.getOptionValue("db");
        }
        catch(Exception ex) {
            fatal(new TestConfigIssue( "Missing option! " + ex.toString()));
            System.out.println("Missing Option!" + ex.toString());
            System.exit(1);
        }
        
        if(cmd.hasOption("c")) { configFile = cmd.getOptionValue("c"); }
        if (cmd.hasOption("elog") && !cmd.getOptionValue("elog").equals("default") ) { elogpwfile = cmd.getOptionValue("elog"); }
        TestAutoPrescaling test = new TestAutoPrescaling(partition, isname, triggerdb, configFile, elogpwfile);
        
        if(cmd.hasOption("reset")) {
            test.reset();
        } else if(cmd.hasOption("isupdate")) {
            while(test.isUpdate()){}
        } else if(cmd.hasOption("block")) {
            test.block();
        } else if(cmd.hasOption("elog")) {
            test.sendElogMsg();
        } else if(cmd.hasOption("sendps")) {
            test.sendNewPSS(cmd.getOptionValue("sendps").equals("exec"));
        }
        System.out.println("Done with main");
    }
}
