package autoprescaleeditor;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.cli.*;

import config.Configuration;

import dal.MasterTrigger;
import dal.RunControlApplicationBase;
import daq.rc.CommandSender;
import ipc.InvalidPartitionException;
import is.Repository;

import TTCInfo.AutoPrescaleCTP; // for APEAbsISListener

import TRIGGER.AutoPrescalerServant;
import TRIGGER.TriggerCommander;
import L1CTIPC.PrescaleWriterServant;

import autoprescaleeditor.menu.L1Prescale;


public class AutoPrescaleEditor {

    // Logger
    private static final APELogger Logger = APELogger.createLogger("AutoPrescaleEditor");

    // Path to store the database downloaded files
    public static String OutputDir = "/logs/AutoPrescaler/";
    
    // IS listener to CTP update requests
    private APEAbsISListener<AutoPrescaleCTP> listener = null;

    // IS communicator to read and publish values
    private APEISReader isReader = null;

    // IS name of the CTP update request object
    private String isname = null;
    
    // Helper to safe prescale sets to the TriggerDB
    private L1PrescaleSaver prescaleSaver = null;

    // partition name and object
    private String partitionName = null;
    private ipc.Partition partition = null;

    private TriggerCommander triggerCommander = null;
    private int baseL1Psk = 0;

    // number of parallel requests
    private int update_request = 0;
    private int reset_request = 0;
    
    // test mode simulates the CTP response by updating the current l1 key in IS
    private boolean testMode = false;
    public void setTestMode(boolean testMode) {
        this.testMode = testMode;

        if(this.testMode) {
            Logger.warningErs(new APELogger.Issue("Setting testmode to " + this.testMode + ". This is ONLY for TESTING, inform Trigger Expert if you see this !"));
        } else {
            Logger.infoErs(new APELogger.Issue("Setting testmode to " + this.testMode));
        }
    }

    private int uploadDelay = 0;
    public void setUploadDelay(int uploadDelay) {
        this.uploadDelay = uploadDelay;
        if(this.uploadDelay>0) {
            Logger.warningErs(new APELogger.Issue("Setting upload delay to " + this.uploadDelay + ". This is ONLY for TESTING, inform Trigger Expert if you see this !"));
        } else {
            Logger.infoErs(new APELogger.Issue("Setting upload delay to " + this.uploadDelay));
        }
    }

    private String blockerName = "";
    private final AtomicBoolean isBlocked = new AtomicBoolean(false);
    private synchronized void blockMe(final String caller) { isBlocked.set(true); blockerName = caller; }
    private synchronized void unblockMe() { isBlocked.set(false); blockerName = ""; }
    private synchronized boolean isblocked() { return isBlocked.get(); }
    private synchronized String blocker() { return blockerName; }

    // counts requests
    private synchronized void new_update_request() { update_request++; }
    private synchronized void end_update_request() { update_request--; }
    private synchronized int number_update_requests() { return update_request; }

    private synchronized void new_reset_request() { reset_request++; }
    private synchronized void end_reset_request() { reset_request--; }
    private synchronized int number_reset_requests() { return reset_request; }

    private AutoPrescalerServant servant = null;

    private PrescaleWriterServant prescaleServant = null;
    
    /**
     * Constructor 
     *
     * @param partitionName - name of partition to that APE should be running with
     * @param isname - IS repository name
     * @param triggerdb - Trigger database alias
     * @param configfile - File with items' names, used in extractItemNames - NOT USED CURRENTLY 
     *                          - to be changed to has a loist of forbidden items
     */
    public AutoPrescaleEditor( final String servantName, 
                               final String partitionName,
                               final String isname,
                               final String triggerdb,
                               final String configfile) throws InvalidPartitionException, InterruptedException
    {
        this.partitionName = partitionName;
        this.partition     = new ipc.Partition(partitionName);
        this.isReader      = new APEISReader(this.partition);
        this.isname        = isname;

        Logger.infoErs(new APELogger.Issue("Trigger Level 1 Autoprescaler started in non-interactive mode\nPartition: " + partitionName 
                       + " \nServant: " + servantName
                       + "\nIS name: " + isname
                       + "\nItem config file: " + configfile
                       + "\nversion: " + AutoPrescaleEditor.class.getPackage().getImplementationVersion()
                       ));

        this.servant = new AutoPrescalerServant(partition, servantName, new AutoPrescalerServant.AutoPrescalerOperations() {
            @Override
            public void hold(String caller) throws Exception {
                Logger.infoErs(new APELogger.ControlIssue("Holding Autoprescaler on request from " + caller));
                blockMe(caller);
            }
            @Override
            public void resume(String caller) throws Exception {
                Logger.infoErs(new APELogger.ControlIssue( "Resuming Autoprescaler on request from " + caller));
                unblockMe();
            }
        });

        // Add the new PSKWriter servant:
        this.prescaleServant = new PrescaleWriterServant(partition, servantName+"Writer", new PrescaleWriterServant.PrescaleWriterOperations() {
            @Override
            public L1CTIPC.PrescaleReturn requestPrescale(final L1CTIPC.PrescaleRequest request) throws L1CTIPC.InvalidName,L1CTIPC.ObsoletePsk {
                // Just a wrapper for an internal function (!!)
                try {
                    L1CTIPC.PrescaleReturn myReturn = requestNewPrescaleKey(request);
                    Logger.debug("Returning: no exceptions yet. Succes: " + myReturn.success + " Psk: " + myReturn.psk);
                    return new L1CTIPC.PrescaleReturn(myReturn.success, myReturn.psk);
                }
                catch (InterruptedException ex) {
                    // SMK problem: return a failure
                    return new L1CTIPC.PrescaleReturn(false, -1);
                }

            }
	    });        

        final ipc.Partition p = new ipc.Partition(partitionName);

	    // Parameters: Database, Items allowed to change, whether a confirmation is required
        prescaleSaver = new L1PrescaleSaver(triggerdb, new ArrayList<String>()); // TODO pass forbiddenItems list
        prescaleSaver.configureWithSMK(isReader.getSMKFromIS());
        
        getMasterTrigger();
    }


    /**
     * Handle any request received by the AutoPrescaleCTP listener
     */
    private UpdateInfoT<AutoPrescaleCTP> updater = new UpdateInfoT<AutoPrescaleCTP>()
    {
        @Override
        public void updateInfo(final AutoPrescaleCTP info) {
            Logger.debug("Calling updateInfo - deprecated"); // updater is not used anywhere
            if(info.L1_PSK_reset>0) {
                if(number_reset_requests()>0) { return; }
                new_reset_request();
                handleResetRequest();
                end_reset_request();
            } else {
                if(number_update_requests()>0) { return; }
                new_update_request();
                handlePrescaleRequest(info);
                end_update_request();
            }
        }
    };


    /**
     * Handles received reset request
     *
     * Reset the key to the current L1 psk currently defined as 
     * default STANDBY or PHYSICS L1 psk
     */
    private void handleResetRequest()
    {
        Logger.debug("Calling handleResetRequest - deprecated"); // called only by updater
        int resetL1PskTarget = isReader.getL1PskForCurrentStateFromIS();
        if( resetL1PskTarget == 0) { return; }
        
        int currentL1Psk = isReader.getCurrentL1PskFromIS();
        if( resetL1PskTarget == currentL1Psk) {
            Logger.info("Abort reset request since the reset target key already matches current prescale " + currentL1Psk);
            return;
        }

        Logger.warningErs(new APELogger.PSIssue("End of hot L1 trigger period, resetting L1 PSK to default " + isReader.getCurrentStateFromIS() + " L1 PSK " + resetL1PskTarget));
        applyToCTP(resetL1PskTarget);
    }    


    /* 
     * Test function which we should disable if we don't want bad things to happen online
     */
    public void singleItemPrescale(final String trigName, int validPSK, int prescaleValue) throws L1CTIPC.InvalidName,L1CTIPC.ObsoletePsk
    {
        Logger.debug("Calling singleItemPrescale - deprecated"); // called by tests
        L1CTIPC.PrescaleRequest request = new L1CTIPC.PrescaleRequest();
        request.psk = validPSK;
        L1CTIPC.PrescaleValue value = new L1CTIPC.PrescaleValue();
        value.item = trigName;
        value.prescale = prescaleValue;
        L1CTIPC.PrescaleValue[] valueArray = {value};
        request.values = valueArray;
        
        Logger.info("Requesting a new prescale in singleItemPrescale");
        try{
            requestNewPrescaleKey(request);
        } catch(InterruptedException ex) {
		    Logger.warningErs(new APELogger.Issue("Problem encountered:"));
            Logger.logStacktrace(ex);
	    }
    }
    
    
    /**
     * Read and validate the new IPC (Corba) request
     *
     * @param request Request
     *
     * @return IPC (Corba) response: success flag and new key
     */
    private L1CTIPC.PrescaleReturn requestNewPrescaleKey(final L1CTIPC.PrescaleRequest request) throws L1CTIPC.InvalidName,L1CTIPC.ObsoletePsk,InterruptedException
    {
        // Our requests do not come SMKs, so we need to
        // retrieve it from IS: This is probably overkill...
        this.baseL1Psk  = request.psk;
        Logger.info("Received new prescale request. The key is " + request.psk + " size of the request " + request.values.length);

        // Use the SMK set-up by hand as a test
        prescaleSaver.configureWithSMK(isReader.getSMKFromIS());

        // We will need to create a L1PSK with only
        // modifications to the last loaded one
        L1Prescale pss = prescaleSaver.readL1PSS(this.baseL1Psk);
        L1Prescale pssRef = new L1Prescale(pss);
        pssRef.setId(this.baseL1Psk);

        // And here we modify only the items we care about...
        // To prevent multiple loops, only loop once over all entries
        for (L1CTIPC.PrescaleValue myValue : request.values) {
            boolean nameFound = false;
            for (int ctpid = 0; ctpid < L1Prescale.MAX_LENGTH; ctpid++) {
                String item_name = prescaleSaver.getCtpIdToItemNameMap().get(ctpid);
                if (myValue.item.equals(item_name)){
                    nameFound = true;
                    int cut = L1Prescale.validateCutFromCTP(L1Prescale.calculateCutFromPrescale((double) myValue.prescale));
                    Logger.debug("Name found " + item_name 
                                    + " Target: " + myValue.item
                                    + " CTPID: " + ctpid
                                    + " PS: " + myValue.prescale
                                    + " Old PS: " + pss.getCutValue(ctpid)
                                    + " Cut: " + cut);

                    pss.setCutValue(ctpid, cut);
                }
            }	    
            if (!nameFound) {
                Logger.errorErs(new APELogger.PSIssue("Prescale Request entry " + myValue.item + " not found in the L1 prescale set"));
                throw new L1CTIPC.InvalidName("Invalid Trigger Item Name", myValue.item);
            }
        }	    
        
        Logger.info("Validating the new prescales set...");
        L1PrescaleSaver.ValidationResult validationResult = prescaleSaver.validateNewPrescales(pss, pssRef);
        if (!L1PrescaleSaver.isPrescaleSetValid(validationResult)){
            Logger.errorErs(new APELogger.PSIssue( "Auto prescale request from CTP not valid: " + request.toString()));
            return new L1CTIPC.PrescaleReturn(false, -1);   
        }

        Logger.info("Saving the new prescales set to the database...");
        int l1psk = saveToTriggerDB(validationResult.getPrescaleSet());
        if (l1psk <= 0) { // no prescale key saved
            Logger.errorErs(new APELogger.PSIssue("New prescale key upload failed/aborted"));
            throw new L1CTIPC.ObsoletePsk("Obsolete PSK value", request.psk);
            //return new L1CTIPC.PrescaleReturn(false, -1);
        }

        Logger.info("Returning success with the new prescale key " + l1psk);
        return new L1CTIPC.PrescaleReturn(true, l1psk);
    }
    
    /**
     * Handle received prescale request
     */
    private void handlePrescaleRequest(final AutoPrescaleCTP ctpinfo)
    {
        Logger.debug("Calling handlePrescaleRequest - deprecated"); // called only by the updater
        Logger.info("A change in IS for AutoPrescaleCTP has been detected");

        // received prescale request
        synchronized(isBlocked) {
            if (isblocked()) {
                Logger.infoErs(new APELogger.ControlIssue( "Autoprescale request ignored, since auto prescaler is blocked by " + blocker() ));
                return;
            }
        }

        this.baseL1Psk  = ctpinfo.L1_PSK_current;
        prescaleSaver.configureWithSMK(ctpinfo.SMK_current);
        
        L1Prescale pssRef = prescaleSaver.readL1PSS(baseL1Psk);
        L1Prescale pss = new L1Prescale(pssRef);

        int ctpid=0;
        for(long ps : ctpinfo.L1_Prescales) {
            pss.setCutValue(ctpid++, (int)ps);
        }

        L1PrescaleSaver.ValidationResult validationResult = prescaleSaver.validateNewPrescales(pss, pssRef);
        if (!L1PrescaleSaver.isPrescaleSetValid(validationResult)){
            Logger.errorErs(new APELogger.PSIssue("Auto prescale request from CTP not valid"));
            return;   
        }
        
        int l1psk = saveToTriggerDB(pss);
        if( l1psk <= 0 ) { // no prescale key saved
            return;
        }

        // simulate slow trigger DB upload to see how a intermittend warm-start would affect this
        if(this.uploadDelay > 0) {
            Logger.warningErs(new APELogger.PSIssue("Delaying submit of l1 psk " + l1psk + " to CTP by " + this.uploadDelay + " seconds !"));

            try {
                Thread.sleep(1000*this.uploadDelay);
            }
            catch(InterruptedException ex) {
                Logger.warningErs(new APELogger.Issue("Exception when upload delay"));
            }
        }

        // check if L1 psk has changed since request was issued
        int currentL1Psk = isReader.getCurrentL1PskFromIS();
        if( currentL1Psk != baseL1Psk ) { 
            Logger.warningErs(new APELogger.PSIssue("Current L1 PS key " + currentL1Psk + " has changed since request was issued ( " + this.baseL1Psk + "). Cancel auto-prescale request!"));
            return;
        }

        synchronized(isBlocked) {
            if(isblocked()) {
                Logger.infoErs(new APELogger.ControlIssue( "Autoprescale request aborted, since auto prescaler has been blocked in the meantime by " + blocker() ));
                return;
            }
        }
        
        // send to CTP
        Logger.infoErs(new APELogger.PSIssue("Sending autoprescale key " + l1psk + " to CTP."));
        boolean success = applyToCTP(l1psk);
        if(success) {
            if(validationResult.getState() == L1PrescaleSaver.RequestState.VALID_HIGH_RATE ) {
                String msg = "L1 prescales automatically changed due to very high rate! Please inform trigger, LAr, and Tile shifters. PSK " + l1psk + "(" + validationResult.getPrescaleSet().getName() + ") -> ";
                msg += validationResult.getPrescaleSet().getComment();
                Logger.errorErs(new APELogger.PSIssue( msg));
            } else {
                String msg = "L1 prescales automatically changed due to high rate! PSK " + l1psk + "(" + validationResult.getPrescaleSet().getName() + ") -> ";
                msg += validationResult.getPrescaleSet().getComment();
                Logger.warningErs(new APELogger.PSIssue( msg));
            }
        }
    }

    /**
     * Save passed L1Prescale object to the database via prescale saver.
     * Performs additional check for the L1 prescale key:
     *   if it has changed since the start of processing the request abort
     *
     * @param pss Prescale set to save
     *
     * @return Prescale key of newly inserted set
     */
    private int saveToTriggerDB(final L1Prescale pss)
    {
        Logger.info("Requested to save a new prescale set based on L1 PSK: " + this.baseL1Psk);
        int currentL1Psk = isReader.getCurrentL1PskFromIS();
        Logger.debug("Current L1 psk: " + currentL1Psk);
        if( currentL1Psk != this.baseL1Psk ) { 
            Logger.warningErs(new APELogger.PSIssue("Current L1 PS key " + currentL1Psk + " has changed since request was issued ( " 
                                                + this.baseL1Psk + "). Cancel auto-prescale request!"));
            return 0;
        }
        int newL1Psk = prescaleSaver.saveNewL1PrescaleSet(pss);
        Logger.debug("New L1 PSK: " + newL1Psk);
        return newL1Psk;
    }

    
    private boolean applyToCTP(final int newL1Psk)
    {
        Logger.debug("Calling applyToCTP - deprecated"); // called by updater
        if(this.testMode) {
            return simulateApplyToCTP(newL1Psk);
        }
        try{
            triggerCommander.setL1Prescales(newL1Psk);
        } catch (Exception ex) {
            Logger.errorErs(new APELogger.PSIssue("Exception when sending L1 PSK " + newL1Psk + " to CTP. (" + ex.toString() + ")"));
            return false;
        }

        final String controller = "RootController";
        final String command    = "LUMIBLOCKINCREASE";
        final String arguments  = "";
        CommandSender commander  = new CommandSender(this.partition.getName(), "IGUITriggerPanel");

        Logger.info("Send run-control command \"" + command + "\" with arguments \"" + arguments + "\" to controller \"" + controller + "\"");
        try {
            commander.userCommand(controller, command, new String[]{});
            Logger.infoErs(new APELogger.LBIssue( "Issued LUMIBLOCK increase"));
        }
        catch(Exception e) {
            Logger.errorErs(new APELogger.LBIssue( "Could not increase LUMIBLOCK"));
            return false;
        }

        return true;
    }

    private boolean simulateApplyToCTP(final int newL1Psk)
    {
        Logger.debug("Calling simulateApplyToCTP - deprecated"); // simulate...
        Logger.warningErs(new APELogger.SimIssue( "AutoPrescaler in test mode, CTP not called!"));
        sendL1PSKToIS(newL1Psk);
        return true;
    }    
    
     /**
     * Publish L1 Prescale Set key to the IS: RunParams.TrigConfL1PsKey
     *
     * @param key Key to publish
     */ 
    private void sendL1PSKToIS(final int key)
    {
        Logger.debug("Calling sendL1PSKToIS - deprecated"); // it is called by simulateApplyToCTP
        Repository isRepository = new Repository(this.partition);
        Logger.debug("Sending L1 PSK " + key +  " to IS");
        final String infoname = "RunParams.TrigConfL1PsKey";
        TrigConfL1PsKeyNamed isl1key = new TrigConfL1PsKeyNamed(this.partition, infoname);
        isl1key.L1PrescaleKey = key;
        try {
            isRepository.checkin( infoname, isl1key);
        }
        catch(is.RepositoryNotFoundException ex) {
            Logger.errorErs(new APELogger.ISIssue("Did not find is repository. Can't publish L1 PSK"));
        }
        catch(is.InfoNotCompatibleException exe) {
            Logger.errorErs(new APELogger.ISIssue("Trying to write wrong type to IS"));
        }
    }
    
    // Read items that are allowed to change from the config file
    // TODO - change to read forbidden items
    public static List<String> extractItemNames(final String configfile)
    {
        List<String> items = new ArrayList<String>();
        try {
            File f = new File(configfile);
            BufferedReader reader = new BufferedReader(new FileReader(f));
            String line = null;
            while ((line=reader.readLine()) != null) {
                Scanner linescanner = new Scanner(line);
                try {
                    items.add(linescanner.next());
                }
                catch(Exception ex) {}
            }
            reader.close();
        }
        catch(FileNotFoundException ex) {
            Logger.fatalErs(new APELogger.ConfigIssue("Could not find '" + configfile + "'. " + ex.toString()));
            System.exit(1);
        }
        catch (IOException ex) {
            Logger.fatalErs(new APELogger.ConfigIssue("Could not read " + configfile + " " + ex.toString()));
            System.exit(1);
        }
        Logger.debug("In '" + configfile + "' found these items that are allowed to change: " + items.toString());
        return items;
    }

    
    private void getMasterTrigger()
    {
        String mtControllerName = null;
        try {
            Configuration db = new Configuration("rdbconfig:RDB@" + this.partitionName);
            dal.Partition dalPartition = dal.Partition_Helper.get(db, partitionName);

            final MasterTrigger mt = dalPartition.get_MasterTrigger();
            if(mt == null) {
                Logger.errorErs(new APELogger.Issue("The Master Trigger is not defined in the partition; it will not be possible to hold or resume the trigger"));
                return;
            }             
                
            final RunControlApplicationBase rc = mt.get_Controller();
            if(rc == null) {
                Logger.errorErs(new APELogger.Issue("The Master Trigger does not define a controller; it will not be possible to hold or resume the trigger"));
                return;
            }

            mtControllerName = rc.UID();
            Logger.info("The partition Master Trigger is " + mtControllerName);
            this.triggerCommander = new TriggerCommander(this.partition, mtControllerName);
        }
        catch(final Exception ex) {
            Logger.errorErs(new APELogger.Issue("Exception when looking for the Master Trigger: " + ex));
        }
    }


    public static void main( String[] args ) throws InterruptedException, InvalidPartitionException {

        String servant    = "L1AutoPrescaler";
        String partition  = "ATLAS";
        String isname     = "L1CT.AutoPrescaleCTP";
        String triggerdb  = "TRIGGERDB_RUN3";
        String configfile = "/det/ctp/MaxItemRates.txt";
        int smk = 0;
        int uploaddelay = 0;
        
        CommandLine cmd = null;
        Options options = new Options();

        try {
            CommandLineParser parser = new DefaultParser(); // PosixParser();
            options.addOption("h", false, "help");
            options.addOption("s", true, "servant name ["+servant+"]");
            options.addOption("p", true, "partition name ["+partition+"]");
            options.addOption("n", true, "is object name ["+isname+"]");
            options.addOption("db", true, "triggerdb ["+triggerdb+"]");
            options.addOption("c", true, "config file ["+configfile+"]");
            options.addOption("dbPath", false, "path to save database output files ["+AutoPrescaleEditor.OutputDir+"]");
            options.addOption("confirm", false, "confirmation required");
            options.addOption("testmode", false, "testmode simulates CTP response");
            options.addOption("uploaddelay", true, "simulates slow upload (delay in seconds [" + uploaddelay + "])");
            cmd = parser.parse(options, args);
        } catch (ParseException ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("SendL1Pss2IS <options>", options);
            System.exit(1);
        }
        if (cmd.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("SendL1Pss2IS <options>", options);
            System.exit(0);
        }
        if (cmd.hasOption("s"))   { servant    = cmd.getOptionValue("s"); }
        if (cmd.hasOption("p"))   { partition  = cmd.getOptionValue("p"); }
        if (cmd.hasOption("n"))   { isname     = cmd.getOptionValue("n"); }
        if (cmd.hasOption("db"))  { triggerdb  = cmd.getOptionValue("db"); }
        if (cmd.hasOption("c"))   { configfile = cmd.getOptionValue("c"); }
        if (cmd.hasOption("dbPath"))   { AutoPrescaleEditor.OutputDir = cmd.getOptionValue("dbPath"); }
        if (cmd.hasOption("uploaddelay")){ uploaddelay = Integer.valueOf(cmd.getOptionValue("uploaddelay")); }

        boolean testmode  = cmd.hasOption("testmode");

        AutoPrescaleEditor ape = new AutoPrescaleEditor( servant, partition, isname, triggerdb, configfile);
        if(testmode) { ape.setTestMode(testmode); }
        if(uploaddelay>0) { ape.setUploadDelay(uploaddelay); }
        
        Logger.info("Ready and listening!");
	
	// Hard-coded test...
	/*
	try{
	    ape.singleItemPrescale("L1_J30_EMPTY", 25, 125);
	} catch(L1CTIPC.InvalidName|L1CTIPC.ObsoletePsk|SQLException ex) { IguiLogger.info("Caught...");}
	
	IguiLogger.info("NewPSKMade?");
	*/	

	// Catch...
        Object o = new Object();
        synchronized (o) {
            while (true) {
                o.wait(500);
            }
        }
    }
}    
